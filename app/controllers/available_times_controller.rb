class AvailableTimesController < ApplicationController
  # GET /available_times
  def index
    set_available_times
  end

  # PUT /available_times
  # NOTE: Works only when there already are AvailableTimes created, so #index
  # should create instead of initialize for first run.
  def update
    transaction = ActiveRecord::Base.transaction do
      available_times_params["available_times"].each do |key, vs|
        available_time = AvailableTime.find(key)
        available_time.update(vs) || set_available_times &&
          raise(ActiveRecord::Rollback)
      end
    end
    transaction ? redirect_to(available_times_path) : render(:index)
  end

  private
  def available_times_params
    params.permit(available_times: [:week_day, :starts_on, :ends_on])
  end

  def set_available_times
    @available_times = []
    appointment_slots_ids = []
    (0..6).each do |i|
      available_time = AvailableTime.find_or_initialize_by(week_day: i)
      @available_times << available_time
      appointment_slots_ids << AppointmentSlot.available.in_week_day(i)
        .during(available_time.starts_on, available_time.ends_on).map(&:id)
    end
    appointment_slots_ids = appointment_slots_ids.flatten.uniq
    @appointment_slots = AppointmentSlot.where(id: appointment_slots_ids)
  end
end
