##
# These are the hours selected by the user in which they have availability to
# take +Appointment+s.
#
# NOTE: Ruby doesn't have a 'time of day' type, so even though db stores times,
# Rails will spit datetimes. The date part of +starts_on+ and +ends_on+ must be
# ignored, and +week_day+ must be used instead.
##
class AvailableTime < ApplicationRecord
  include DayTimesValidatable
end
