##
# +Appointment+s happen in available +AppontmentSlot+s that occur during
# +AvailableTime+s.
#
# NOTE: Ruby doesn't have a 'time of day' type, so even though db stores times,
# Rails will spit datetimes. The date part of +starts_on+ and +ends_on+ must be
# ignored, and +week_day+ must be used instead.
##
class AppointmentSlot < ApplicationRecord
  include DayTimesValidatable

  has_one :appointment

  scope :available, -> { where(available: true) }
  scope :in_week_day, -> (day) { where(week_day: day) }
  scope :during, -> (from, to) do
    where("starts_on BETWEEN :from AND :to AND ends_on BETWEEN :from AND :to",
      from: from, to: to)
  end

  validates :available, inclusion: { in: [true, false] }
end
