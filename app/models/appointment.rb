##
# When an +AppointmentSlot+ is available and within a user's +AvailableTime+,
# they can pick it to create a schedule. Once they do, the +AppointmentSlot+
# becomes unavailable.
##
class Appointment < ApplicationRecord
  belongs_to :appointment_slot

  # +:create+ context is important, since slots will be unavailable after they
  # are selected for an appointment
  validate :appointment_slot_is_available, on: :create

  after_create_commit :make_appointment_slot_unavailable

  private
  def appointment_slot_is_available
    if appointment_slot.present? && !appointment_slot.available
      errors.add(:appointment_slot, :invalid)
    end
  end

  def make_appointment_slot_unavailable
    appointment_slot && appointment_slot.update!(available: false)
  end
end
