module DayTimesValidatable
  extend ActiveSupport::Concern

  included do
    validates :starts_on, :ends_on, :week_day, presence: true
    validates :week_day, numericality: {
      greater_than_or_equal_to: 0,
      less_than_or_equal_to:    6
    }
    validate :positive_duration

    private
    def positive_duration
      if starts_on.present? && ends_on.present? && ends_on <= starts_on
        errors.add(:ends_on, :invalid)
      end
    end
  end
end
