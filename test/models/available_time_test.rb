require 'test_helper'

class AvailableTimeTest < ActiveSupport::TestCase
  test "fixture is valid" do
    available_time = available_times(:available_time_one)
    assert available_time.valid?, available_time.errors.inspect
  end

  test "available times can be created" do
    available_time = AvailableTime.new(starts_on: "14:35", ends_on: "15:20",
      week_day: 0)
    assert available_time.save, available_time.errors.inspect
  end

  test "validations work" do
    available_time = AvailableTime.new
    assert_not available_time.valid?

    assert_errors_removed(available_time, :starts_on, :blank) do
      available_time.update(starts_on: '14:35')
    end

    assert_errors_removed(available_time, :ends_on, :blank) do
      available_time.update(ends_on: '14:35') # invalid duration
    end

    assert_errors_removed(available_time, :week_day, :blank) do
      available_time.update(week_day: 8) # invalid day
    end

    assert_errors_removed(available_time, :ends_on, :invalid) do
      available_time.update(ends_on: '15:35')
    end

    assert_errors_removed(available_time, :week_day,
      :less_than_or_equal_to) do
      available_time.update(week_day: -1)
    end

    assert_errors_removed(available_time, :week_day,
      :greater_than_or_equal_to) do
      available_time.update(week_day: 1)
    end

    assert available_time.valid?, available_time.errors.inspect
  end
end
