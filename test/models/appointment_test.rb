require 'test_helper'

class AppointmentTest < ActiveSupport::TestCase
  test "fixture is valid" do
    appointment = appointments(:appointment_one)
    assert appointment.valid?, appointment.errors.inspect
  end

  test "appointments can be created" do
    appointment_slot = appointment_slots(:appointment_slot_two)
    appointment = Appointment.new(appointment_slot: appointment_slot)
    assert appointment.save, appointment.errors.inspect
  end

  test "appointment slot association" do
    appointment = appointments(:appointment_one)
    appointment_slot = appointment_slots(:appointment_slot_one)
    assert_equal appointment_slot, appointment.appointment_slot
  end

  test "validations work" do
    appointment = Appointment.new
    assert_not appointment.valid?

    appointment_slot = appointment_slots(:appointment_slot_one)
    assert_not appointment_slot.available?
    assert_errors_removed(appointment, :appointment_slot, :blank) do
      appointment.update(appointment_slot: appointment_slot) # invalid
    end

    appointment_slot = AppointmentSlot.new(starts_on: "04:40", ends_on: "04:45",
      week_day: 4)
    assert appointment_slot.available
    assert appointment_slot.save, appointment_slot.errors.inspect
    assert_errors_removed(appointment, :appointment_slot, :invalid) do
      appointment.update(appointment_slot: appointment_slot)
    end

    assert appointment.valid?, appointment.errors.inspect
  end

  test "makes slot unavailable after creation" do
    appointment_slot = AppointmentSlot.new(starts_on: "00:10", ends_on: "00:15",
      week_day: 5)
    assert appointment_slot.available
    assert appointment_slot.save, appointment_slot.errors.inspect

    appointment = appointment_slot.build_appointment
    assert appointment.save, appointment.errors.inspect

    assert_not appointment_slot.reload.available
  end
end
