require 'test_helper'

class AppointmentSlotTest < ActiveSupport::TestCase
  test "fixture is valid" do
    appointment_slot = appointment_slots(:appointment_slot_one)
    assert appointment_slot.valid?, appointment_slot.errors.inspect
  end

  test "appointment slots can be created" do
    appointment_slot = AppointmentSlot.new(starts_on: "14:35", ends_on: "15:20",
      week_day: 0, available: true)
    assert appointment_slot.save, appointment_slot.errors.inspect
  end

  test "available defaults to true" do
    appointment_slot = AppointmentSlot.new
    assert appointment_slot.available
  end

  test "validations work" do
    appointment_slot = AppointmentSlot.new
    assert_not appointment_slot.valid?

    assert_errors_removed(appointment_slot, :starts_on, :blank) do
      appointment_slot.update(starts_on: '14:35')
    end

    assert_errors_removed(appointment_slot, :ends_on, :blank) do
      appointment_slot.update(ends_on: '14:35') # invalid duration
    end

    assert_errors_removed(appointment_slot, :week_day, :blank) do
      appointment_slot.update(week_day: 8) # invalid day
    end

    appointment_slot.available = nil # set by default
    assert_errors_removed(appointment_slot, :available, :inclusion) do
      appointment_slot.update(available: true)
    end

    assert_errors_removed(appointment_slot, :ends_on, :invalid) do
      appointment_slot.update(ends_on: '15:35')
    end

    assert_errors_removed(appointment_slot, :week_day,
      :less_than_or_equal_to) do
      appointment_slot.update(week_day: -1)
    end

    assert_errors_removed(appointment_slot, :week_day,
      :greater_than_or_equal_to) do
      appointment_slot.update(week_day: 1)
    end

    assert appointment_slot.valid?, appointment_slot.errors.inspect
  end

  test "appointment association" do
    appointment_slot = appointment_slots(:appointment_slot_one)
    appointment = appointments(:appointment_one)
    assert_equal appointment, appointment_slot.appointment
  end

  test "available scope" do
    appointment_slot = appointment_slots(:appointment_slot_one)
    assert_not appointment_slot.available

    available = AppointmentSlot.available
    assert available.any?
    assert available.all? { |as| as.available }
    assert_not available.include?(appointment_slot)
  end

  test "in_week_day scope" do
    appointment_slot = appointment_slots(:appointment_slot_one)
    assert_equal 1, appointment_slot.week_day

    in_week_day = AppointmentSlot.in_week_day(1)
    assert in_week_day.any?
    assert in_week_day.all? { |as| as.week_day == 1 }
    assert in_week_day.include?(appointment_slot)

    appointment_slot = appointment_slots(:appointment_slot_two)
    assert_not_equal 1, appointment_slot.week_day
    assert_not in_week_day.include?(appointment_slot)
  end

  test "during scope" do
    appointment_slot = appointment_slots(:appointment_slot_one)
    start_time = appointment_slot.starts_on - 5.minutes
    end_time = appointment_slot.ends_on + 5.minutes

    during = AppointmentSlot.during(start_time, end_time)
    assert during.include?(appointment_slot)

    appointment_slot_two = appointment_slots(:appointment_slot_two)
    assert appointment_slot_two.starts_on < start_time
    assert_not during.include?(appointment_slot_two)
  end

  test "chained scopes" do
    assert appointment_slot = AppointmentSlot.create(starts_on: "10:00",
      ends_on: "10:30", week_day: 0, available: true)

    assert AppointmentSlot.available.in_week_day(0).include?(appointment_slot)
    assert_not AppointmentSlot.available.in_week_day(1)
      .include?(appointment_slot)

    start_time = appointment_slot.starts_on + 4.minutes
    end_time = appointment_slot.ends_on - 4.minutes
    assert_not AppointmentSlot.during(start_time, end_time)
      .include?(appointment_slot)
    assert_not AppointmentSlot.available.in_week_day(0)
      .during(start_time, end_time).include?(appointment_slot)
    start_time = appointment_slot.starts_on
    end_time = appointment_slot.ends_on
    assert AppointmentSlot.available.in_week_day(0)
      .during(start_time, end_time).include?(appointment_slot)
  end
end
