ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  ##
  # Helper for checking if an +ApplicationRecord+ error disappears after
  # performing a given block. Performs assertions on validity before and after
  # the block is yielded.
  #
  # @param [ApplicationRecord] record An ApplicationRecord to check for errors
  # @param [Symbol] attribute The record's attribute to check errors from
  # @param [Symbol] msg The error's message to be checked
  # @param [Symbol] context The context in which validations are checked.
  # @param [Hash] opts If the error has some options, pass them.
  #
  # @example
  #   assert_errors_removed(user, :name, :blank) do
  #     user.update(name: 'name') # or user.name = 'name'
  #   end
  ##
  def assert_errors_removed(record, attribute, msg, context = nil, opts = {})
    record.valid?(context)
    fail_message = "#{attribute} was expected to have an error marked as " \
    "#{msg} but was not. #{record.errors.inspect}"
    assert record.errors.added?(attribute, msg, opts), fail_message

    yield

    record.valid?(context)
    fail_message = "#{attribute} was NOT expected to have an error marked as " \
    "#{msg} but was. #{record.errors.inspect}"
    assert_not record.errors.added?(attribute, msg, opts), fail_message
  end
end
