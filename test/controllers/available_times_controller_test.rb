require 'test_helper'

class AvailableTimesControllerTest < ActionDispatch::IntegrationTest
  test "available times can be seen" do
    get available_times_path
    assert_response :success
  end
end
