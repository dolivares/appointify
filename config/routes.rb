Rails.application.routes.draw do
  resources :available_times, only: [:index] do
    collection do
      patch "/", action: :update
    end
  end

  root "available_times#index"
end
