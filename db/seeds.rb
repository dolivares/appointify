AvailableTime.destroy_all

(0..6).each do |i|
  AvailableTime.create!(
    [
      week_day: i,
      starts_on: "00:00",
      # Current workaround for required times validation
      ends_on: "00:01"
    ]
  )

  AppointmentSlot.create!(
    [
      {
        week_day: i,
        starts_on: "08:00",
        ends_on: "09:00"
      },
      {
        week_day: i,
        starts_on: "09:00",
        ends_on: "10:00"
      },
      {
        week_day: i,
        starts_on: "10:00",
        ends_on: "11:00"
      }
    ]
  )
end
