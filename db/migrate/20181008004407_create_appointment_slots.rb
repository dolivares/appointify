class CreateAppointmentSlots < ActiveRecord::Migration[5.2]
  def change
    create_table :appointment_slots do |t|
      t.time :starts_on
      t.time :ends_on
      t.integer :week_day
      t.boolean :available, default: true

      t.timestamps
    end
  end
end
