class CreateAppointments < ActiveRecord::Migration[5.2]
  def change
    create_table :appointments do |t|
      t.references :appointment_slot, foreign_key: true

      t.timestamps
    end
  end
end
