class CreateAvailableTimes < ActiveRecord::Migration[5.2]
  def change
    create_table :available_times do |t|
      t.integer :week_day
      t.time :starts_on
      t.time :ends_on

      t.timestamps
    end
  end
end
